<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Coinmarketcap | Dashboard</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    @yield('header')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link rel="icon" href="{{ asset('img/logo.png')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .img-circle {
            width: 50px;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        @if($url != 'main')
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            {{-- <span>
                                <img alt="image" class="img-circle" src="{{Auth::user()->ava_src}}" width="40%" />
                            </span> --}}
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">
                                    <span class="block m-t-xs">
                                        <strong class="font-bold">{{-- Auth::user()->name --}}</strong>
                                    </span>
                                    <span class="text-muted text-xs block">Admin
                            </a>
                        </div>
                        <div class="logo-element">
                            Coinmarketcap
                        </div>
                    </li>
                    <li class="{{Route::current()->getName() == 'order' ? 'active' : ''}}">
                        <a href="/"><i class="fa fa-bar-chart"></i> <span class="nav-label">Index</span></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            @yield('content')
        </div>
        @else
            @yield('content')
        @endif



    </div>
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <!-- <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script> -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('js/demo/sparkline-demo.js') }}"></script>
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js')}}"></script>

    @yield('footer')
</body>

</html>
