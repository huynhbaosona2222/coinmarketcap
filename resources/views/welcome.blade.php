@extends('layouts.app')
@section('content')
<style>
.error__show {
    height: 250px;
    overflow-y: scroll;
    border: 1px solid #676a6c;
    padding: 7px;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Clone Coinmarketcap

        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Test display data 123(by {{$user->name}})</a>
            </li>
            <li>
                <a href="#">Coinmarketcap</a>
            </li>
            <li class="active">
                <strong>Clone Coinmarketcap</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>symbol</th>
                            <th>Change 24h</th>
                            <th>Change 7d</th>
                            <th>Change 14d</th>
                            <th>Change 30d</th>
                            <th>Change 60d</th>
                            <th>Change 200d</th>
                            <th>Change 1y</th>
                        </tr>
                    </thead>
                        <tbody>
                        @foreach ($data as $key => $d)
                            <tr class="gradeX">
                                <td>{{$d->market_data->market_cap_rank}}</td>
                                <td><img src="{{$d->image->thumb}}" alt="">{{$d->name}}</td>
                                <td style=" text-transform: uppercase;">{{$d->symbol}}</td>
                                <td class="center">{{$d->market_data->price_change_percentage_24h}}</td>
                                <td class="center">{{$d->market_data->price_change_percentage_7d}}</td>
                                <td class="center">{{$d->market_data->price_change_percentage_14d}}</td>
                                <td class="center">{{$d->market_data->price_change_percentage_30d}}</td>
                                <td class="center">{{$d->market_data->price_change_percentage_60d}}</td>
                                <td class="center">{{$d->market_data->price_change_percentage_200d}}</td>
                                <td class="center">{{$d->market_data->price_change_percentage_1y}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
</div>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src="js/plugins/dataTables/datatables.min.js"></script>

<script>

$(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 300,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            var list = $(".center");
            for (i = 0; i < list.length; i++){
                item = $(list[i]).html();
                if (item < 0) {
                    console.log($(list[i]));
                    $(list[i]).attr("style","color:red");
                } else {
                    $(list[i]).attr('style', "color:green")
                }
            }

        });
</script>
@endsection
