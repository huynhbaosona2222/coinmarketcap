<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class MainController extends Controller
{


    public function index()
    {
        $user = User::find(1);
        $result = [];
        $arr = [1, 2, 3, 4, 5, 6];
        foreach ($arr as $a) {
            $link = 'https://api.coingecko.com/api/v3/coins?page='.$a;
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => $link,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            foreach ($a = json_decode($response) as $res) {
                // $check = $res->market_data->price_change_percentage_200d;
                // if (1 < $check && $check < 500) {
                // if ($check < 500) {
                //     $new['symbol'] = $res->symbol;
                //     $new['name'] = $res->name;
                //     $new['change_in_1_y'] = $check;
                    // $result[] = $res;
                // }
                $result[] = $res;
            }
        }
        return view('welcome')->with([
            'data' => $result,
            'url' => 'main',
            'user' => $user
        ]);
    }
}
