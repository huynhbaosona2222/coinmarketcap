<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1 ,
                'name' => 'Sonhuynh',
                'email'=> 'hello@pixiostudio.com',
                // 'phone_number' => '84373205443',
                // 'ava_src' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg',
                'password'=>'$2y$10$egpKAKvCPvY4jGh5Qy6G0uqWYWhxu8/L2Hz/KkIMIn4aDrPvSUMVO',
                // 'role_id' => 1,
            ],
        ]);
    }
}
